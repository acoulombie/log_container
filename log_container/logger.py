import logging
from pprint import pprint

from docker import Client
cli = Client(base_url='tcp://172.17.42.1:2375')

container_name = 'nostalgic_lumiere'

stats_gen = cli.stats(container_name)

if stats_gen:
    while True:
        try:
            #pprint(gen_stats.next())
            logging.basicConfig(filename='stats.log',level=logging.INFO)
            logging.info(stats_gen.next())
        except StopIteration:
            break
